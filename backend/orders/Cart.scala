package mua.orders

import cats.data.ValidatedNel
import eu.timepit.refined.types.numeric.{NonNegInt, PosInt}
import eu.timepit.refined.cats.syntax._
import io.circe.generic.JsonCodec
import io.circe.generic.extras.ConfiguredJsonCodec
import io.circe.refined._
import io.getquill.Embedded
import mua.auth.{GeoAddress, User}
import mua.products.Product
import mua.util.json.configs.withDefaults
import mua.orders
import io.scalaland.chimney.dsl._
import mua.inventory.Location
import mua.pricing.{Currency, Price}
import mua.util.types.Label

@ConfiguredJsonCodec case class Cart(
  creatorId: User.ID,
  buyerId: Option[User.ID] = None,
  locationId: Option[Location.ID] = None,
  currency: Option[Currency] = None,
  shippingAddress: Option[GeoAddress] = None,
  lineItems: List[Cart.LineItem] = List.empty
)

object Cart {

  @JsonCodec case class DTO(
    items: List[Cart.LineItemDTO],
    shippingAddress: Option[GeoAddress] = None
  )

  @ConfiguredJsonCodec
  case class LineItem(productId: Product.ID, size: Label, quantity: NonNegInt = NonNegInt(0)) extends Embedded {
    def toOrdered(price: Price.Amount): ValidatedNel[String, orders.LineItem] =
      PosInt.validate(quantity.value).map(orders.LineItem(productId, _, size, price))
  }

  @ConfiguredJsonCodec
  case class LineItemDTO(productId: Product.ID, size: Label, quantity: NonNegInt = NonNegInt(0)) {
    def toModel: LineItem = {
      this.into[LineItem]
        .transform
    }
  }

  object LineItemDTO {
    def fromModel(li: LineItem): LineItemDTO =
      li.into[LineItemDTO]
        .transform
  }
}