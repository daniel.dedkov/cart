package mua.orders

import cats.data.NonEmptyList
import cats.syntax.all._
import cats.instances.list._
import monix.eval.Task
import mua.auth.{Context, GeoAddress, User, UserService}
import mua.app.base.dao.Transactor
import mua.i18n
import mua.inventory.InventoryService
import mua.pricing.{Price, PricingService}
import mua.util.syntax._
import mua.products.{Product, ProductService}
import mua.util.errors.{ElementNotFound, NotPermitted}
import mua.util.types.Label


class CartService(
  dao: OrderDAO,
  userSvc: UserService,
  productsSvc: ProductService,
  pricingSvc: PricingService,
  inventorySvc: InventoryService,
  transact: Transactor
) {

  // TODO: rethink includeZeroQuantity. Try to find more clean solution for this duality
  def getCart(includeZeroQuantity: Boolean = true, ctx: Context): Task[Cart] =
    ctx.ifNotGuest(dao.getCart(includeZeroQuantity, ctx.user.id))

  def upsertItem(lineItem: Cart.LineItem, ctx: Context): Task[Unit] =
    ctx.ifNotGuest(dao.upsertItem(ctx.user.id, lineItem))

  def removeItem(productId: Product.ID, size: Option[Label] = None, ctx: Context): Task[Unit] =
    ctx.ifNotGuest(dao.deleteItem(ctx.user.id, productId, size))

  def updateAddress(newAddress: GeoAddress, ctx: Context): Task[Unit] =
    dao.upsertAddress(ctx.user.id, newAddress)

  def clear(ctx: Context): Task[Unit] =
    ctx.ifNotGuest(dao.clearCart(ctx.user.id))

  def submit(ctx: Context): Task[Cart] = ctx.ifNotGuest(transact {
      for {
        cart <- getCart(includeZeroQuantity = false, ctx)
        buyerId = cart.buyerId.getOrElse(ctx.user.id)
        buyer <- userSvc.getById(buyerId)

        _ <- Task.raiseError(NotPermitted(s"Can not find buyer info")).whenA(buyer.isEmpty)
        _ <- Task.raiseError(NotPermitted(i18n.MissingShippingAddress(ctx.lang))).whenA(cart.shippingAddress.isEmpty)

        priceMode = pricingSvc.getPriceMode(buyer.get)
        shippingAddress = cart.shippingAddress.get

        oCtx = Context.Ordering(
          buyer.get,
          cart.locationId.getOrElse(ctx.locationId),
          shippingAddress,
          cart.currency.getOrElse(ctx.currency),
          priceMode
        )

        ord <- populateOrder(cart, ctx, oCtx)
        _ <- dao.saveOrder(ord)
        _ <- clear(ctx)
      } yield ()
    }, Some(i18n.CanNotSubmitOrderAnonymously(ctx.lang))
  ).map(_ => Cart(ctx.user.id))


  private def populateOrder(cart: Cart, ctx: Context, oCtx: Context.Ordering): Task[Order] =
    for {
      lineItems <- cart.lineItems
        .traverse(fillOrderData(_, oCtx))
        .hasMany("Line items")
        .map(_.sequence)
        .requireValid(i18n.EmptyLineItems(ctx.lang))
      productIds = lineItems.map(_.productId).toList.distinct

      // TODO make more subtle check for
      _ <- Task.raiseError(ValidationFailed(i18n.OrderHasUnavailableItems(ctx.lang)).whenA(inventorySvc.checkAvailability(lineItems))
//      availability <- inventorySvc.getProductsAvailability(NonEmptyList.fromListUnsafe(productIds), ctx)
      products <- productIds.traverse(productsSvc.getForId(_, oCtx.buyer)).hasMany("Line items")
      order <- Order.create(cart.creatorId, products, lineItems, oCtx)
    } yield order

  private def fillOrderData(li: Cart.LineItem, oCtx: Context.Ordering) =
    for {
      price <- pricingSvc.getOrderingPrice(li.productId, oCtx.locationId, oCtx.currency, oCtx.priceMode)
      orderItem = li.toOrdered(price)
    } yield orderItem

}
