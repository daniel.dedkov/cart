package mua.orders

import eu.timepit.refined.types.numeric.PosInt
import io.circe.generic.JsonCodec
import mua.products.Product
import mua.util.types.Label
import io.circe.refined._
import mua.pricing.Price

@JsonCodec
case class LineItem(
  productId: Product.ID,
  quantity: PosInt,
  size: Label,
  price: Price.Amount
)
