package mua.orders

import cats.data.NonEmptyList
import io.circe.generic.JsonCodec
import monix.eval.Task
import mua.auth.{Context, GeoAddress, User}
import mua.util.{OwnIdType, currentTime}
import mua.util.json.codecs._
import java.time.Instant

import mua.auth.validated.Username
import mua.inventory.Location
import mua.pricing.Currency
import mua.pricing.Price.Amount
import mua.products.Product


@JsonCodec
case class Order(
  id: Order.ID,
  creatorId: User.ID,
  buyerId: User.ID,
  buyerName: Username,
  createdOn: Instant,
  locationId: Location.ID,
  status: OrderStatus,
  currency: Currency,
  shippingAddress: GeoAddress,
  lineItems: NonEmptyList[LineItem],
  products: NonEmptyList[Product]
)

object Order extends OwnIdType {
  def create(
    creatorId: User.ID,
    products: NonEmptyList[Product],
    lineItems: NonEmptyList[LineItem],
    oCtx: Context.Ordering
  ): Task[Order] = currentTime.map(now =>
    Order(
      Order.ID.Unknown,
      creatorId,
      oCtx.buyer.id,
      oCtx.buyer.name,
      now,
      oCtx.locationId,
      OrderStatus.Review,
      oCtx.currency,
      oCtx.shippingAddress,
      lineItems,
      products
    )
  )
}