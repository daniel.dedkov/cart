package mua.orders

import cats.implicits._
import io.circe.generic.JsonCodec
import mua.app.base.controllers.{BaseController, Endpoints, ResponsesToJSON}
import mua.app.modules.AppConfig
import mua.auth.GeoAddress
import mua.products.Product
import mua.util.types.Label

class OrderController (
  orderService: OrderService,
  cartService: CartService,
  appConfig: AppConfig
) extends BaseController("/orders")(appConfig) with ResponsesToJSON {
  @JsonCodec case class Status(status: OrderStatus)

  override protected def endpoints: Endpoints = {
    case GET -> Root as ctx =>
      for {
        orders <- orderService.get(ctx = ctx)
        response <- Ok(orders)
      } yield response

    case GET -> Root / Order.ID(orderId) as ctx =>
      Ok(orderService.getById(orderId, ctx = ctx))

    case GET -> Root / "cart" as ctx =>
      Ok(cartService.getCart(includeZeroQuantity = true, ctx).map(populateDTO))

    case r @ PUT -> Root / "cart" as ctx =>
      for {
        lineItems <- r.req.parse[List[Cart.LineItemDTO]]
        _ <- lineItems.traverse(dto => cartService.upsertItem(dto.toModel, ctx))
        response <-NoContent()
      } yield response

    case r @ PUT -> Root / "cart" / "address" as ctx =>
      for {
        address <- r.req.parse[GeoAddress]
        _ <- cartService.updateAddress(address, ctx)
        response <-NoContent()
      } yield response

    case DELETE -> Root / "cart" / Product.ID(productId) as ctx =>
      cartService.removeItem(productId, None, ctx)
        .flatMap(_ => NoContent())

    case DELETE -> Root / "cart" / Product.ID(productId) / Label(size) as ctx =>
      cartService.removeItem(productId, Some(size), ctx)
        .flatMap(_ => NoContent())

    case DELETE -> Root / "cart" as ctx =>
      cartService.clear(ctx).flatMap(_ => NoContent())

    case POST -> Root / "cart" / "submit" as ctx =>
      for {
        cart <- cartService.submit(ctx)
        response <- Ok(populateDTO(cart))
      } yield response

    case r @ PUT -> Root / Order.ID(id) / "status" as ctx =>
      Ok {
        r.req.parse[Status].map(_.status)
          .flatMap(orderService.updateStatus(id, _, ctx))
      }
  }

  private def populateDTO(cart: Cart): Cart.DTO = {
    Cart.DTO(items = cart.lineItems.map(Cart.LineItemDTO.fromModel), shippingAddress = cart.shippingAddress)
  }

}
