package mua.orders

import cats.implicits._
import monix.eval.Task
import mua.auth.{GeoAddress, User}
import mua.app.base.dao.QueryInterface
import mua.inventory.Location
import mua.pricing.Currency
import mua.products.Product
import mua.util.types.Label
import io.scalaland.chimney.dsl._


class OrderDAO (db: QueryInterface) {
  import OrderDAO.CartData
  import OrderDAO.CartItem

  import db._

  private val cartData = quote { query[CartData] }
  private val cartItems = quote { query[CartItem] }

  // CART

  private def userLineItems(userId: User.ID) = quote {
    cartItems.filter(_.creatorId == lift(userId))
  }

  private def userWOData(userId: User.ID) = quote {
    cartData.filter(_.creatorId == lift(userId))
  }

  def getCart(includeZeroQuantity: Boolean = true, userId: User.ID): Task[Cart] = {
    run {
      userWOData(userId)
        .leftJoin(cartItems).on((data, item) => data.creatorId == item.creatorId)
    }.map(list => {
      val maybeData = list.headOption.map(_._1)
      val items = list.flatMap(_._2)
      maybeData match {
        case Some(data) => data
          .into[Cart]
          .withFieldConst(_.lineItems, items.map(_.item).filter(item => includeZeroQuantity || item.quantity.value > 0))
          .transform
        case None => Cart(userId)
      }
    })
  }

  def getLineItem(userId: User.ID, productId: Product.ID): Task[Option[Cart.LineItem]] =
    run(userLineItems(userId).filter(_.item.productId == lift(productId)).map(_.item)).map(_.headOption)

  def deleteItem(userId: User.ID, productId: Product.ID, size: Option[Label] = None): Task[Unit] = {
    val q = quote {userLineItems(userId).filter(_.item.productId == lift(productId))}

    if (size.isEmpty) {
      run(q.delete).void
    } else {
      run(q.filter(_.item.size == lift(size.get)).delete).void
    }
  }

  def upsertItem(userId: User.ID, lineItem: Cart.LineItem): Task[Unit] = transaction {
    val insert = run(cartItems.insert(lift(CartItem(userId, lineItem))))
    val update = run(userLineItems(userId)
      .filter(_.item.productId == lift(lineItem.productId))
      .filter(_.item.size == lift(lineItem.size))
      .update(_.item.quantity -> lift(lineItem.quantity))
    )

    for {
      p <- run(userLineItems(userId)
        .filter(_.item.productId == lift(lineItem.productId))
        .filter(_.item.size == lift(lineItem.size))
      )
      _ <- run(cartData
        .insert(lift(CartData(userId)))
        .onConflictIgnore
      )
      _ <- if (p.isEmpty) insert else update
    } yield ()
  }

  def clearCart(userId: User.ID): Task[Unit] = {
    run(userLineItems(userId).delete).void *>
      run(userWOData(userId).delete).void
  }

  def upsertAddress(userId: User.ID, newAddress: GeoAddress): Task[Unit] =
    run(userWOData(userId).update(_.shippingAddress -> Some(lift(newAddress)))).void

  // leaving it here for the case if we will merge anonymous and persisted sessions in the future
  def updateCreator(currentOwnerId: User.ID, newOwnerId: User.ID): Task[Unit] = transaction {
    val alreadyOrderedItems = quote {
      userLineItems(newOwnerId).map(e => (e.item.productId, e.item.size))
    }

    for {
      _ <- run(userLineItems(currentOwnerId).filter(e => alreadyOrderedItems.contains((e.item.productId, e.item.size))).delete).void
      _ <- run(userLineItems(currentOwnerId).update(_.creatorId -> lift(newOwnerId))).void
    } yield ()
  }

  // ORDERS

  def getOrders(
    ids: Option[List[Order.ID]] = None,
    buyerId: Option[User.ID] = None
  ): Task[List[Order]] = run {
    val idsQ = liftQuery(ids.getOrElse(List()))

    query[Order]
      .filter(o => lift(ids.isEmpty) || idsQ.contains(o.id))
      .filter(o => lift(buyerId).forall(_ == o.buyerId))
      .sortBy(_.createdOn)(Ord.desc)
  }

  def setStatus(id: Order.ID, status: OrderStatus): Task[Unit] = run {
    query[Order]
      .filter(_.id == lift(id))
      .update(_.status -> lift(status))
  }.void

  def saveOrder(order: Order): Task[Order] =
    run(query[Order].insert(lift(order)).returning(_.id)).map(order.copy(_))

  def updateOrder(order: Order): Task[Unit] =
    run(query[Order].filter(_.id == lift(order.id)).update(lift(order))).void
}

object OrderDAO {
  // Normalized model for cart line items
  private case class CartItem(creatorId: User.ID, item: mua.orders.Cart.LineItem)

  private case class CartData(
    creatorId: User.ID,
    buyerId: Option[User.ID] = None,
    locationId: Option[Location.ID] = None,
    currency: Option[Currency] = None,
    shippingAddress: Option[GeoAddress] = None
  )

}