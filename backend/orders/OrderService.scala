package mua.orders

import monix.eval.Task
import mua.auth.Context
import mua.app.base.dao.Transactor
import mua.orders.OrderStatus._
import mua.util.errors.NotPermitted
import mua.util.syntax._

class OrderService (dao: OrderDAO, transact: Transactor) {
  def get(ids: Option[List[Order.ID]] = None, ctx: Context): Task[List[Order]] =
    dao.getOrders(ids, buyerId = if (ctx.isMega) None else Some(ctx.user.id))

  def getById(id: Order.ID, ctx: Context): Task[Order] =
    get(Some(List(id)), ctx).hasOne(s"Order id $id")

  def updateStatus(id: Order.ID, to: OrderStatus, ctx: Context): Task[Order] =
    transact {
      for {
        order  <- get(Some(List(id)), ctx).hasOne(s"Order id $id")
        _      <- validateTransition(order, to, ctx)
        update =  order.copy(status = to)
        _      <- dao.updateOrder(update)
      } yield update
    }


  private def validateTransition(order: Order, to: OrderStatus, ctx: Context) =
    Task.eval {
      val isCreator = ctx.user.id == order.creatorId

      (order.status, to) match {
        case _ if ctx.isMega =>
          // TODO: maybe nobody should be able to un-cancel an order?
        case (Review, Cancelled) if isCreator =>

        case (from, _) =>
          throw NotPermitted(
            s"Cannot progress order ${order.id} from $from to $to", Some(ctx.user.id))
      }
    }
}
