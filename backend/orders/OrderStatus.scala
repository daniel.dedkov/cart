package mua.orders

import com.olegpy.enumdsl._
import mua.util.EnumExtras


@enum(mix[EnumExtras])
trait OrderStatus {
  val Review, Reserved, Shipped, Done, Cancelled = Value
}
