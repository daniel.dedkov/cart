import { Product, ProductID, Products } from '../product/Product'
import { Container } from '../container/Container'
import { Ref, Size, valueOf } from '../base/Types'
import _ from '../util/mudash'
import { Money, PriceSelector } from '../product/LocationPrices'
import { GeoAddress } from '../user/GeoAddress'

// TODO use this class def when it relates to calling API
export class LineItemDTO {
    productId: ProductID
    size: Size
    quantity: number

    constructor(obj: any) {
        this.productId = obj.productId || obj.product.id
        this.size = obj.size
        this.quantity = obj.quantity
    }
}

export class CartLineItem extends Ref {
    product: Product
    size?: Size
    quantity?: number

    constructor(product: Product, size?: Size, quantity?: number) {
        super(product)
        this.product = product
        this.size = size || Size.ANY
        this.quantity = quantity
    }

    getPrice(priceSettings: PriceSelector): Money {
        return Products.cost(this.product, priceSettings).actual
    }
}

export class Cart extends Container<CartLineItem> {
    shippingAddress: GeoAddress

    constructor({items = [], shippingAddress = {}}: {items?: CartLineItem[], shippingAddress?: GeoAddress}) {
        // we do not create line items here cause raw cart data
        // does not contain products, but only product ids
        // So, lets delegate correct data population on those who call ctor
        super()
        this.shippingAddress = shippingAddress || {}
        this.items = [...(items || [])]
    }

    itemsEqual(a: CartLineItem, target: CartLineItem): boolean {
        return valueOf(a) === valueOf(target) && (a.size === target.size)
    }

    contains(item: CartLineItem): boolean {
        return item.quantity !== 0 && super.contains(item)
    }

    containsProduct(product: Product): boolean {
        return this.items.some(x => valueOf(x) === valueOf(product))
    }

    get productIdsToLineItems(): Map<ProductID, CartLineItem[]> {
        const map = new Map()
        this.items.forEach(li => li.product && (map.has(li.product.id) ? map.get(li.product.id).push(li) : map.set(li.product.id, [li])))
        return map
    }

    get size(): number {
        return _.uniqBy(this.items, 'product.id').length
    }

    totalPrice(priceSettings: PriceSelector): number {
        const total = this.items.reduce((acc, li: CartLineItem) => {
            const price = li.getPrice(priceSettings) * li.quantity
            return acc + price
        }, 0)

        return total
    }

    totalQuantity(): number {
        const total = this.items.reduce((acc, li: CartLineItem) => {
            return acc + li.quantity
        }, 0)

        return total
    }

}
