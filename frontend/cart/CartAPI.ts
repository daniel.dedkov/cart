import { Cart, CartLineItem, LineItemDTO } from './Cart'
import { HTTP } from '../http/HTTP'
import _ from '../util/mudash'
import { LocalStorage } from '../util/LocalStorage'
import { SingletonContainerDataProviderOnWeb } from '../container/SingletonContainerDataProviderOnWeb'
import { SingletonContainerDataProviderOnLocalStorage } from '../container/SingletonContainerDataProviderOnLocalStorage'
import { SingletonContainerDataProvider } from '../container/SingletonContainerDataProvider'
import { GeoAddress } from '../user/GeoAddress'

export interface CartAPI extends SingletonContainerDataProvider<CartLineItem> {
    updateAddress(address: GeoAddress): Promise<Cart>
    submitCart(): Promise<Cart>
}

export class CartWebAPI extends SingletonContainerDataProviderOnWeb<CartLineItem> implements CartAPI {

    constructor() {
        super('/orders/cart')
    }

    async submitCart(): Promise<Cart> {
        return HTTP.post(`${this.root}/submit`)
    }

    async updateAddress(address: GeoAddress): Promise<Cart> {
        return HTTP.put(`${this.root}/address`, address)
    }

    async upsertMany(container: Cart, items: CartLineItem[]): Promise<Cart> {
        const payload = items.map(item => new LineItemDTO(item))
        await HTTP.put(`${this.root}`, payload)
        items.forEach(item => container.upsert(item))
        return container
    }

    async upsertItem(cart: Cart, item: CartLineItem): Promise<Cart> {
        await HTTP.put(`${this.root}`, [new LineItemDTO(item)])
        return cart.upsert(item) as Cart
    }

    async removeItem(cart: Cart, item: CartLineItem): Promise<Cart> {
        if (_.isEmpty(item.size)) {
            await HTTP.delete_(`${this.root}/${item.product.id}`)
            return cart.remove(item, {product: item.product}) as Cart
        } else {
            await HTTP.delete_(`${this.root}/${item.product.id}/${item.size}`)
            return cart.remove(item) as Cart
        }
    }

}

export class CartLocalAPI extends SingletonContainerDataProviderOnLocalStorage<CartLineItem> implements CartAPI {

    constructor() {
        super('mua-cart')
    }

    async submitCart(): Promise<Cart> {
        throw new Error('Can not submit cart if not registered')
    }

    async updateAddress(address: GeoAddress): Promise<Cart> {
        const cart = LocalStorage.get(this.root)
        cart.shippingAddress = address
        LocalStorage.put(this.root, cart)
        return cart as Cart
    }

    async upsertItem(cart: Cart, lineItem: CartLineItem): Promise<Cart> {
        const updated = cart.upsert(lineItem)
        const toPersist = Object.assign({}, updated) as any
        toPersist.items = updated.items.map(item => new LineItemDTO(item))
        LocalStorage.put(this.root, toPersist)
        return updated as Cart
    }

    async upsertMany(cart: Cart, lineItems: CartLineItem[]): Promise<Cart> {
        const updated = await super.upsertMany(cart, lineItems) as any
        const toPersist = Object.assign({}, updated) as any
        toPersist.items = updated.items.map(item => new LineItemDTO(item))
        LocalStorage.put(this.root, toPersist)
        return updated as Cart
    }

    async removeItem(cart: Cart, lineItem: CartLineItem): Promise<Cart> {
        const updated = cart.remove(lineItem, {product: lineItem.product}) as Cart
        const toPersist = Object.assign({}, updated) as any
        toPersist.items = updated.items.map(item => new LineItemDTO(item))
        LocalStorage.put(this.root, toPersist)
        return updated as Cart
    }

}