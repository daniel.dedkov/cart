import * as React from 'react'
import { Action, handleActions } from 'redux-actions'
import { all, call, put, takeLatest } from 'redux-saga/effects'
import { AsyncAction, asyncOperations, SyncAction } from '../app/ActionTypes'
import { getInitialState } from '../app/AppState'
import { registerReduxActions } from '../app/RootReducer'
import { Product } from '../product/Product'
import { ProductsRepository } from '../product/ProductsRepository'
import { Cart, CartLineItem } from './Cart'
import { CartRepository } from './CartRepository'
import { GeoAddress } from '../user/GeoAddress'
import _ from '../util/mudash'

import {
    confirmationCall,
    takeSeq,
    uiBlockingCall,
    watchAsyncAction
} from '../app/AppSagas'

// Actions-types available in the system
export const CART = {
    TO_STATE: 'CART/TO_STATE',
    ASYNC: {
        FETCH: asyncOperations('CART/ASYNC/FETCH'),
        SUBMIT: asyncOperations('CART/ASYNC/SUBMIT'),
        UPDATE_ADDRESS: asyncOperations('CART/ASYNC/UPDATE_ADDRESS'),
        UPSERT_LINE_ITEMS: asyncOperations('CART/ASYNC/UPSERT_LINE_ITEMS'),
        REMOVE_PRODUCT: asyncOperations('CART/ASYNC/REMOVE_PRODUCT'),
    }
}

// Wire in async actions
export function cartSagas() {
    return all([
        watchAsyncAction(takeLatest, uiBlockingCall(FetchCart.saga)),
        watchAsyncAction(takeLatest, SubmitCart),
        watchAsyncAction(takeLatest, UpdateAddress),
        watchAsyncAction(takeLatest, AddToCart),
        watchAsyncAction(takeSeq, confirmationCall(RemoveProductFromCart.saga)),
    ])
}

export const FetchCart: AsyncAction = {
    type: CART.ASYNC.FETCH,

    emit(): Action<{}> {
        return { type: this.type.START }
    },

    *saga() {
        const cartData = yield call(uiBlockingCall(CartRepository.getCurrent))
        const productIds = cartData.items.map(li => li.productId)
        let products = yield call(uiBlockingCall(ProductsRepository.getForIds), productIds)
        products = _.keyBy(products, 'id')
        const getProduct = (id): Product => products[id]
        const lineItems = cartData.items.map(li => new CartLineItem(getProduct(li.productId), li.size, li.quantity))

        const cart = new Cart(_.assign(cartData, {items: lineItems})) // assign shippingAddress and other cart data
        yield put(CartToState.emit(cart))
    },
}

export const SubmitCart: AsyncAction = {
    type: CART.ASYNC.SUBMIT,

    emit(): Action<{}> {
        return { type: this.type.START }
    },

    *saga() {
        const cart =  yield call(uiBlockingCall(CartRepository.submitCart))
        yield put(CartToState.emit(cart))
    },
}

export const UpdateAddress: AsyncAction = {
    type: CART.ASYNC.UPDATE_ADDRESS,

    emit(payload: GeoAddress): Action<GeoAddress> {
        return { type: this.type.START, payload }
    },

    *saga(action: Action<GeoAddress>) {
        const cart = yield call((CartRepository.updateAddress), action.payload)
        yield put(CartToState.emit(cart))
    }
}

interface A {cart: Cart, lineItems: CartLineItem[]}
export const AddToCart: AsyncAction = {
    type: CART.ASYNC.UPSERT_LINE_ITEMS,

    emit(payload: A): Action<A> {
        return { type: this.type.START, payload }
    },

    *saga(action: Action<A>) {
        const {cart, lineItems} = action.payload
        const updatedCart = yield call(uiBlockingCall(CartRepository.upsertMany), cart, lineItems)
        yield put(CartToState.emit(updatedCart))
    }
}

interface B { cart: Cart, product: Product }
export const RemoveProductFromCart: AsyncAction = {
    type: CART.ASYNC.REMOVE_PRODUCT,

    emit(payload: B): Action<B> {
        return { type: this.type.START, payload }
    },

    *saga(action: Action<B>) {
        const {cart, product} = action.payload
        const lineItem = new CartLineItem(product)
        const updatedCart = yield call(uiBlockingCall(CartRepository.removeItem), cart, lineItem)
        yield put(CartToState.emit(updatedCart))
    }
}

export const CartToState: SyncAction = {
    type: CART.TO_STATE,

    emit(payload: Cart): Action<Cart> {
        return { type: this.type, payload}
    },

    reducer(currentState: CartState, action: Action<ReduxPayload>): CartState {
        let nextState = currentState

        if (action.payload) {
            nextState = new Cart({items: action.payload.items, shippingAddress: action.payload.shippingAddress})
        }

        return nextState
    },
}

// Wire in Redux actions
export type CartState = Cart
type ReduxPayload = Cart

export const CartReducer = registerReduxActions<CartState, ReduxPayload> (
    [
        CartToState,
    ],
    getInitialState().cart
)
