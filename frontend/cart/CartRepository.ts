import API from '../http/API'
import { bindAll } from '../util/general'
import { SingletonContainerRepository } from '../container/SingletonContainerRepository'
import { Cart, CartLineItem } from './Cart'
import { CartAPI } from './CartAPI'
import { GeoAddress } from '../user/GeoAddress'

@bindAll
class CartRepositoryImpl extends SingletonContainerRepository<CartLineItem> {

    makeContainer({items = [], shippingAddress = {}}: {items?: Array<CartLineItem>, shippingAddress?: GeoAddress}): Cart {
        return new Cart({items, shippingAddress})
    }

    public async submitCart(): Promise<Cart> {
        await (this.dataProvider as CartAPI).submitCart() // TODO typecasting
        return new Cart({})
    }

    public async updateAddress(address: GeoAddress): Promise<Cart> {
        return (this.dataProvider as CartAPI).updateAddress(address) // TODO typecasting
    }

}

export const CartRepository = new CartRepositoryImpl(API.cartLocalAPI)
