import {
    isValid,
    Validatable,
    validate,
    validateLength,
    validateNonEmpty,
    validateNumber,
    validateText
} from '../util/validators'
import { i18n } from '../base/i18n'

export interface PaymentInfo extends Validatable {
    number?: string
    date?: string
    holder?: string
    cvs?: string
}

export const paymentInfoValidator = (info: PaymentInfo): PaymentInfo => {
    return {
        number: validateNonEmpty(info.number) || validateCardNumber(info.number),
        date:  validateNonEmpty(info.date) || validateCardDate(info.date),
        holder: validateNonEmpty(info.holder) || validateText(info.holder),
        cvs: validateNonEmpty(info.cvs) || validateNumber(info.cvs) || validateLength(info.cvs, 3, 3),
    }
}

export const isPaymentInfoValid = (info: PaymentInfo): boolean => {
    return isValid(info, paymentInfoValidator)
}

export const validatePaymentInfo = (info: PaymentInfo, checkNulls?: boolean): PaymentInfo => {
    return validate(info, paymentInfoValidator, checkNulls)
}

export const validateCardNumber = (value: string): string => {
    const v = (value || '').replace('\\D', '')
    const regex = new RegExp('^[0-9]{16}$')
    if (!regex.test(v)) {
        return i18n.WrongCardNumber.toString()
    }

    return luhnCheck(v) ? '' : i18n.WrongCardNumber.toString()
}

function luhnCheck(val: string): boolean {
    let sum = 0
    for (let i = 0; i < val.length; i++) {
        let intVal = parseInt(val.substr(i, 1), 10)
        if (i % 2 === 0) {
            intVal *= 2
            if (intVal > 9) {
                intVal = 1 + (intVal % 10)
            }
        }
        sum += intVal
    }
    return (sum % 10) === 0
}

export const validateCardDate = (value: string): string => {
    const v = value || ''

    if (v.match(/^(0\d|1[0-2])\/\d{2}$/)) {
        const { 0: month, 1: year } = v.split('/').map(x => parseInt(x, 10))
        const expiry = new Date(2000 + year, month)
        const now = new Date()
        return expiry.getTime() > now.getTime() ? '' : i18n.CardExpired.toString()
    } else {
        return i18n.WrongInput.toString()
    }
}
