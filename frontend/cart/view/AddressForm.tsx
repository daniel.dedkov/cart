import * as React from 'react'
import { i18n } from '../../base/i18n'
import { GeoAddress } from '../../user/GeoAddress'
import _ from '../../util/mudash'
import { FormInputText } from './FormInputText'

interface Props {
    address: GeoAddress
    errors: GeoAddress
    onPropertyChange?: any
    onPropertySet?: any
    onDone?: any
}

interface State {
}

export class AddressForm extends React.PureComponent<Props, State> {

    render() {
        const {address, errors, onPropertyChange = _.noop, onPropertySet = _.noop} = this.props

        return (
            <div data-uk-grid={true}>
                <FormInputText className="uk-width-1-1" label={i18n.Phone} value={address.phone} onChange={(e, v) => onPropertyChange('phone', v)} onDone={onPropertySet} errorText={errors.phone} maxLen="20" />
                <FormInputText className="uk-width-1-1" required={true} label={i18n.Email} value={address.email} onChange={(e, v) => onPropertyChange('email', v)} onDone={onPropertySet} errorText={errors.email} maxLen="50" />

                <FormInputText className="uk-width-1-2" required={true} label={i18n.Country} value={address.country} onChange={(e, v) => onPropertyChange('country', v)} onDone={onPropertySet} errorText={errors.country} maxLen="50" />
                <FormInputText className="uk-width-1-2" required={true} label={i18n.City} value={address.city} onChange={(e, v) => onPropertyChange('city', v)} onDone={onPropertySet} errorText={errors.city} maxLen="50" />

                <FormInputText className="uk-width-1-1" required={true} label={i18n.Street} value={address.street} onChange={(e, v) => onPropertyChange('street', v)} onDone={onPropertySet} errorText={errors.street} maxLen="100" />

                <FormInputText className="uk-width-1-5" required={true} label={i18n.House} value={address.building} onChange={(e, v) => onPropertyChange('building', v)} onDone={onPropertySet} errorText={errors.building} maxLen="10" />
                <FormInputText className="uk-width-1-5" label={i18n.Flat} value={address.room} onChange={(e, v) => onPropertyChange('room', v)} errorText={errors.room} onDone={onPropertySet} maxLen="10" />
                <FormInputText className="uk-width-2-5" label={i18n.Zip} value={address.zip} onChange={(e, v) => onPropertyChange('zip', v)} errorText={errors.zip} onDone={onPropertySet} maxLen="10" />
                <FormInputText className="uk-width-1-5" label={i18n.State} value={address.state} onChange={(e, v) => onPropertyChange('state', v)} onDone={onPropertySet} errorText={errors.state}  maxLen="50" />

                <FormInputText className="uk-width-1-1" label={i18n.Other} value={address.other} onChange={(e, v) => onPropertyChange('other', v)} onDone={onPropertySet} maxLen="300" />
            </div>
        )
    }
}
