import { createStyles, withStyles } from '@material-ui/core'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormGroup from '@material-ui/core/FormGroup'
import TextField from '@material-ui/core/TextField'
import { $, css } from 'glamor'
import Checkbox from 'material-ui/Checkbox'
import * as React from 'react'
import { ButtonClose } from '../../_views/components/buttons/ButtonClose'
import { AppStore } from '../../app/AppStore'
import { i18n } from '../../base/i18n'
import { Size } from '../../base/Types'
import { GalleryItemCard } from '../../gallery/view/GalleryItemCard'
import { hasSomeAvailable, Inventory } from '../../product/Inventory'
import { Product } from '../../product/Product'
import { User } from '../../user/User'
import { formatPrice, log } from '../../util/general'
import _ from '../../util/mudash'
import { Cart, CartLineItem } from '../Cart'
import { AddToCart, RemoveProductFromCart } from '../CartActions'

interface State {
}

interface Props {
    cart: Cart
    user: User
}

export class CartItemsList extends React.PureComponent<Props, State> {

    handleRemoveAllSizes = (p: Product) => {
        AppStore.Instance.dispatch(RemoveProductFromCart.emit({cart: this.props.cart, prduct: p}))
    }

    onLineItemUpdated = (li: CartLineItem) => {
        AppStore.Instance.dispatch(AddToCart.emit({cart: this.props.cart, lineItems: [li]}))
    }

    render() {
        const {cart, user} = this.props

        const productIdsToLineItems = cart.productIdsToLineItems
        const productIds = Array.from(productIdsToLineItems.keys())
        const products = productIds.map(productId => _.head(productIdsToLineItems.get(productId)).product)
        const productsCount = _.size(products)
        const totalUnits = cart.totalQuantity()

        const productsRendered = products.map(prod => (
            <div className="uk-margin" key={`${prod.id}`}>
                <div className="uk-grid zero" data-uk-grid={true}>
                    <div className="uk-width-auto zero image">
                        <GalleryItemCard hoverable={false} icons={false} product={prod} showPriceFor={user} />
                    </div>
                    <div className="uk-width-expand">
                        <h3 className="uk-card-title uk-margin-remove-bottom">{prod.name.toString()}</h3>
                        <p className="uk-text-meta uk-margin-remove-top">{prod.code}</p>
                        <div className="uk-text-meta uk-margin-remove-top">
                            <ProductSizeSelectors onChange={this.onLineItemUpdated} cart={cart} product={prod} user={user} />
                        </div>
                    </div>
                    <div className="uk-width-auto removeProduct">
                        <ButtonClose onClick={() => this.handleRemoveAllSizes(prod)} tooltip={i18n.RemoveFromCart.toString()} />
                    </div>
                </div>
            </div>
        ))

        if (productsCount) {
            return (
                <div className="zero" {...productListStyles}>
                    <div>{i18n.NProductsInCart(productsCount).toString()}</div>
                    <hr />
                    <div>{productsRendered}</div>
                    <hr />
                    <div>{i18n.TotalQuantity.toString()}: {`${totalUnits}${i18n.Pcs}`}</div>
                    <div>{`${i18n.OrderTotal.toString()}: ${formatPrice(cart.totalPrice(user), user.currency)}`}</div>
                </div>
            )
        } else {
            return (
                <div className="uk-card-header">
                    {i18n.CartIsEmpty.toString()}
                    <br /><br />
                </div>
            )
        }
    }
}

function ProductSizeSelectors_ (props: {cart: Cart, onChange: (item: CartLineItem) => void, product: Product, user: User, classes?: any}) {
    const {cart, product, user, onChange, classes} = props
    const isAvailable = hasSomeAvailable(product, user.locationId)

    if (isAvailable) {
        const items = cart.productIdsToLineItems.get(product.id)

    const updateLineItem = (li: CartLineItem, val: string | number) => {
        const q = val.toString() === '' ? 0 : parseInt(val.toString(), 10)
        if (q >= 0) {
            li.quantity = q
            onChange(li)
        }
    }

    const sizesView = product.availability
        .filter(availability => availability.locationId === user.locationId && availability.size !== Size.ANY)
        .map((inv: Inventory, i) => {
            const li = _.find(items, item => item.size === inv.size) || new CartLineItem(product, inv.size, 0)
            const isOverOrder = li.quantity > inv.available
            const isSizeAvailable = inv.hasAvailable()
            const isDisabled = !isSizeAvailable && !isOverOrder
            return user.isPowered() ?
                (
                    <TextField
                        key={`${product.id}${i}${li.size}`}
                        type="number"
                        classes={{root: classes.textFieldRoot}}
                        disabled={isDisabled}
                        label={`${inv.size} (${inv.available})`}
                        error={isOverOrder}
                        value={li.quantity}
                        onChange={(e: any) => updateLineItem(li, e.target.value)}
                        InputLabelProps={{ shrink: true }}
                        margin="normal"
                        variant="outlined"
                    />
                )
                :
                (
                    <FormControlLabel
                        key={`${product.id}${i}${li.size}`}
                        control={<Checkbox disabled={isDisabled} checked={cart.contains(li)} onClick={() => updateLineItem(li, cart.contains(li) ? 0 : 1)} />}
                        label={inv.size}
                        labelPlacement="start"
                    />
                )
        })

        return (
            <div>
                <FormGroup classes={{root: classes.formGroupRoot}} row={true}>
                    {sizesView}
                </FormGroup>
            </div>
        )
    } else {
        return <div {...soldOutContainerStyles}>{i18n.ProductNotAvailable.toString()}</div>
    }
}

const sizingStyles = createStyles({
    formGroupRoot: {
        margin: '0 0 0 -15px'
    },
    textFieldRoot: {
        width: '75px !important',
        margin: '1em 0 0 1em',
    },
})

export const ProductSizeSelectors = withStyles(sizingStyles)(ProductSizeSelectors_)

const sizeInputContainerStyles = css({
        margin: '1em 0',
    },
    $(' .size-view', {
        float: 'left',
        minWidth: '2em',
        whiteSpace: 'nowrap',
        marginRight: '1em',
    }),
    $(' .label', {
        textAlign: 'center',
        display: 'flex',
    }),
)

const soldOutContainerStyles = css({
        fontSize: '200%',
    },
)

const productListStyles = css({
        margin: '1.2em 0 !important',
    },
    $(' div', {
    }),
    $(' div.removeProduct', {
        margin: '-1em 0 0 0 !important',
    }),
    $(' div.zero', {
        margin: '0 !important',
        padding: '0 !important',
    }),
    $(' div.image', {
        width: '6em',
        maxWidth: '6em',
        minHeight: '8em',
        maxHeight: '8em',
    }),
)
