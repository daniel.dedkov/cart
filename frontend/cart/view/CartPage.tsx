import * as React from 'react'
import { StandardPage, StandardPageProps } from '../../_views/pages/StandardPage'
import { CartItemsList } from './CartItemsList'
import { Loc } from '../../base/Types'
import { i18n } from '../../base/i18n'
import { PageMeta, PageMetaInfo } from '../../_views/pages/PageMeta'
import { Step, Stepper, StepButton, } from 'material-ui/Stepper'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import { $, css } from 'glamor'
import { AppStore } from '../../app/AppStore'
import { SubmitCart, UpdateAddress } from '../CartActions'
import { AddressForm } from './AddressForm'
import _ from '../../util/mudash'
import { validateAddress, GeoAddress, isAddressValid } from '../../user/GeoAddress'
import { actionUserUpdate } from '../../user/UserActions'
import { User } from '../../user/User'
import { Cart } from '../Cart'
import { PaymentForm } from './PaymentForm'
import { clone, log } from '../../util/general'
import Checkbox from 'material-ui/Checkbox'
import { isPaymentInfoValid, PaymentInfo, validatePaymentInfo } from '../PaymentInfo'
import { App } from '../../_views/App'
import { actionAppConfirmationShow } from '../../app/AppActions'

interface Props extends StandardPageProps {}

export class CartPage extends React.PureComponent<Props, {}> {
    render() {
        // TODO [DD]: in teh future cart can be created by mega user for a partner
        // so, we should not take a user from the context, but fetch a buyer user instead
        const { cart, user } = this.props

        return (
            <StandardPage {...this.props}>
                <PageMeta title={CartPageMeta.title} href={CartPageMeta.href} />
                <h1 className="uk-heading-line uk-text-center"><span>{CartPageMeta.title.toString()}</span></h1>
                <CartStepper cart={cart} user={user} />
            </StandardPage>
        )
    }
}

interface StepperProps {
    user: User
    cart: Cart
}

interface StepperState {
    currentStep: string
    steps: string[]
    address: GeoAddress
    addressErrors: GeoAddress
    paymentInfo: PaymentInfo
    paymentInfoErrors: PaymentInfo
    persistUserAddress: boolean
}

class CartStepper extends React.Component<StepperProps, StepperState> {

    CanTryBadAddress: boolean = true
    FullAddressValidation: boolean = false
    FullPaymentInfoValidation: boolean = false

    ProductsStep: string = 'product'
    ShipmentStep: string = 'shipment'
    PaymentStep: string  = 'payment'

    Steps = {
        [this.ProductsStep]: i18n.Goods,
        [this.ShipmentStep]: i18n.ShipmentAndAddress,
        [this.PaymentStep] : i18n.Payment,
    }

    constructor(props: StepperProps) {
        super(props)
        window.scrollTo(0, 0)
        const steps = []
        this.state = {
            steps,
            currentStep: _.first(steps),
            paymentInfo: {},
            paymentInfoErrors: {},
            address: {},
            addressErrors: {},
            persistUserAddress: true,
        }
    }

    componentWillReceiveProps(nextProps: Readonly<StepperProps>, nextContext: any): void {
        const steps = this.availableSteps(nextProps)
        this.setState({
            steps,
            currentStep: this.state.currentStep || _.first(steps),
            address: _.isEmpty(this.state.address) ?
                _.isEmpty(nextProps.cart.shippingAddress) ? nextProps.user.shippingAddress : nextProps.cart.shippingAddress
                : this.state.address,
        })
    }

    onCheckPersistUserAddress = (e: any, v: boolean) => {
        this.setState({persistUserAddress: v})
    }

    sendAddressUpdate = (address: GeoAddress) => {
        const payload: any = _.mapValues(address, v => v === '' ? null : v)
        const isOk = isAddressValid(payload)
        if (isOk) {
            AppStore.Instance.dispatch(UpdateAddress.emit(payload))
            if (this.props.user.isRegistered() && this.state.persistUserAddress) {
                this.props.user.shippingAddress = payload
                AppStore.Instance.dispatch(actionUserUpdate(this.props.user))
            }
        }
    }

    updateAddress = (prop: string, value: any): void => {
        const {address, addressErrors} = this.state
        const obj: any = clone(address)
        obj[prop] = value
        this.setState({address: obj})

        const newErrors = validateAddress(obj, this.FullAddressValidation)
        if (newErrors[prop] !== addressErrors[prop] && !_.isEmpty(addressErrors[prop])) {
            addressErrors[prop] = newErrors[prop]
            this.setState({addressErrors})
        }
    }

    showAddressErrors = (): void => {
        const addressErrors = validateAddress(this.state.address, this.FullAddressValidation)
        this.setState({addressErrors})
    }

    updatePaymentInfo = (prop: string, value: any): void => {
        const {paymentInfo, paymentInfoErrors} = this.state
        const obj: any = clone(paymentInfo)
        obj[prop] = value
        this.setState({paymentInfo: obj})

        const newErrors = validatePaymentInfo(obj, this.FullPaymentInfoValidation)
        if (newErrors[prop] !== paymentInfoErrors[prop] && !_.isEmpty(paymentInfoErrors[prop])) {
            paymentInfoErrors[prop] = newErrors[prop]
            this.setState({paymentInfoErrors})
        }
    }

    showPaymentInfoErrors = (): void => {
        const paymentInfoErrors = validatePaymentInfo(this.state.paymentInfo, this.FullPaymentInfoValidation)
        this.setState({paymentInfoErrors})
    }

    availableSteps = (props) => {
        const { user } = props
        const steps = _.keys(this.Steps).filter(step => {
            if (!user.paymentsEnabled() && step === this.PaymentStep) {
                return false
            } else {
                return true
            }
        })
        return steps
    }

    getStepTab = (step) => {
        return <Step key={`s-${step}`}><StepButton onClick={() => this.setState({currentStep: step})}>{`${this.Steps[step]}`}</StepButton></Step>
    }

    getStepContent(step: string) {
        const {cart, user} = this.props

        switch (step) {
            case this.ProductsStep:
                return <CartItemsList cart={cart} user={user} />
            case this.ShipmentStep:
                return (
                    <div>
                        {user.isRegistered() && <div className="hint uk-width-1-1 uk-padding-small">
                            <Checkbox defaultChecked={this.state.persistUserAddress} label={i18n.WeCanSaveThisAddress.toString()} onCheck={this.onCheckPersistUserAddress} />
                        </div>}
                        <AddressForm
                            onPropertyChange={this.updateAddress}
                            onPropertySet={this.showAddressErrors}
                            address={this.state.address}
                            errors={this.state.addressErrors}
                        />
                    </div>
                )
            case this.PaymentStep:
                return (
                    <div>
                        <div className="hint uk-width-1-1 uk-padding-small">{i18n.WeDoNotStoreYourCardInfo.toString()}</div>
                        <PaymentForm
                            onPropertyChange={this.updatePaymentInfo}
                            onPropertySet={this.showPaymentInfoErrors}
                            info={this.state.paymentInfo}
                            errors={this.state.paymentInfoErrors}
                        />
                    </div>
                )
            default:
                return ''
        }
    }

    isPrevStepPossible = (currStep: string): boolean => {
        return currStep !== _.first(this.state.steps)
    }

    isNextStepPossible = (currStep: string): boolean => {
        return !(
            (currStep === this.ProductsStep && this.props.cart.totalQuantity() === 0)
            || (currStep === this.ShipmentStep && (!isAddressValid(this.state.address) && !this.CanTryBadAddress))
        )
    }

    validateCart = (): Loc[] => {
        const errors = []
        if (this.props.cart.totalQuantity() === 0) {
            errors.push(i18n.MissingQuantities)
        }
        if (!isAddressValid(this.state.address)) {
            errors.push(i18n.AddressInvalid)
        }
        if (this.props.user.paymentsEnabled() && !isPaymentInfoValid(this.state.paymentInfo)) {
            errors.push(i18n.IncorrectPaymentInfo)
        }

        return errors
    }

    handleNext = () => {
        const {currentStep, address, steps} = this.state

        if (currentStep === this.ShipmentStep) {
            if (isAddressValid(address)) {
                this.sendAddressUpdate(address)
            } else {
                // show errors but do not go to next step
                this.FullAddressValidation = true
                this.setState({addressErrors: validateAddress(address, this.FullAddressValidation)})
                this.CanTryBadAddress = false
                return
            }
        }

        if (currentStep === _.last(steps)) {
            const cartErrors = this.validateCart()
            if (_.isEmpty(cartErrors)) {
                AppStore.Instance.dispatch(SubmitCart.emit())
            } else {
                AppStore.Instance.dispatch(actionAppConfirmationShow({
                    show: true,
                    message: <ul>{cartErrors.map((e, i) => <li key={`err-${i}`}>{e.toString()}</li>)}</ul>,
                    title: i18n.CanNotSubmitCart.toString(),
                }))
            }
        }

        window.scrollTo(0, 0)
        this.setState({currentStep: steps[steps.indexOf(currentStep) + 1]})
    }

    handlePrev = () => {
        const {currentStep, steps} = this.state
        if (currentStep !== _.first(steps)) {
            window.scrollTo(0, 0)
            this.setState({currentStep: steps[steps.indexOf(currentStep) - 1]})
        }
    }

    render() {
        const {currentStep, steps} = this.state

        return (
            <div className="uk-width-xxlarge" style={{margin: '0 auto 2em auto', padding: '0 0.5em'}} {...styles}>

                <Stepper linear={false} activeStep={steps.indexOf(currentStep)}>{steps.map(step => this.getStepTab(step))}</Stepper>
                <div>{this.getStepContent(currentStep)}</div>

                <div {...stepperNav}>
                    {currentStep !== _.first(steps) && <FlatButton label={i18n.Back.toString()} disabled={!this.isPrevStepPossible(currentStep)} onClick={this.handlePrev} />}
                    <RaisedButton
                        label={currentStep === _.last(steps) ? i18n.CartDone.toString() : i18n.Proceed.toString()}
                        disabled={!this.isNextStepPossible(currentStep)}
                        onClick={this.handleNext}
                        primary={true}
                    />
                </div>
            </div>
        )
    }
}

const stepperNav = css({
    textAlign: 'right',
    // marginTop: '-2.6em',
})

export const CartPageMeta: PageMetaInfo = {
    href: new Loc('cart'),
    title: i18n.Cart,
}

const styles = css({

    },
    $(' .hint', {
        backgroundColor: '#FDFFC3',
        color: '#5A5B5B',
        borderRadius: '.7em',
        fontSize: '70%',
        margin: '0 auto',
    })
)