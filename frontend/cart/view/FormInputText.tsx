import { Loc } from '../../base/Types'
import * as React from 'react'
import _ from '../../util/mudash'
import TextField from 'material-ui/TextField'

interface TextProps {
    className?: string
    label: Loc
    onChange?: any
    onDone?: any
    value?: string | number
    errorText?: any
    required?: boolean
    maxLen?: string
}

interface TextState {
    isDirty: boolean
}

export class FormInputText extends React.PureComponent<TextProps, TextState> {
    constructor(props: TextProps) {
        super(props)
        this.state = {isDirty: false}
    }

    render() {
        const {className = '', onChange = _.noop, onDone = _.noop, value, label = new Loc(), errorText = '', required = false, maxLen} = this.props

        return (
            <div className={className}>
                <TextField
                    value={value || ''}
                    onChange={onChange}
                    onBlur={onDone}
                    errorText={errorText}

                    maxlength={maxLen}
                    fullWidth={true}
                    floatingLabelText={`${label} ${required ? '*' : ''}`}
                    floatingLabelFocusStyle={hintStyle}
                    underlineFocusStyle={underlineFocusStyle}
                    errorStyle={errorStyle}
                />
            </div>
        )
    }
}

const hintStyle = {
    color: 'pink',
}

const underlineFocusStyle = {
    borderColor: 'pink',
}

const errorStyle = {
    color: 'red',
    borderColor: 'red',
}
