import * as React from 'react'
import { i18n } from '../../base/i18n'
import _ from '../../util/mudash'
import { FormInputText } from './FormInputText'
import { PaymentInfo } from '../PaymentInfo'

interface Props {
    info: PaymentInfo
    errors: PaymentInfo
    onPropertyChange?: any
    onPropertySet?: any
    onDone?: any
}

interface State {
}

export class PaymentForm extends React.PureComponent<Props, State> {

    render() {
        const {info, errors, onPropertyChange = _.noop, onPropertySet = _.noop} = this.props

        return (
            <div data-uk-grid={true}>
                <FormInputText
                    className="uk-width-1-1"
                    required={true}
                    label={i18n.CardNumber}
                    value={info.number}
                    onChange={(e, v) => onPropertyChange('number', v)}
                    onDone={onPropertySet}
                    errorText={errors.number}
                    maxLen="19"
                />
                <FormInputText className="uk-width-2-4" required={true} label={i18n.CartHolder} value={info.holder} onChange={(e, v) => onPropertyChange('holder', v)} onDone={onPropertySet} errorText={errors.holder} maxLen="50" />
                <FormInputText className="uk-width-1-4" required={true} label={i18n.CardDate}   value={info.date} onChange={(e, v) => onPropertyChange('date', v)} onDone={onPropertySet} errorText={errors.date} maxLen="5" />
                <FormInputText className="uk-width-1-4" required={true} label={i18n.CardCVS}    value={info.cvs} onChange={(e, v) => onPropertyChange('cvs', v)} onDone={onPropertySet} errorText={errors.cvs} maxLen="3" />
            </div>
        )
    }
}
